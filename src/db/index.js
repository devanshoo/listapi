const { MongoClient } = require("mongodb");
const url =
    "mongodb+srv://socialpilot:1234567890@socialpilot.qooij.mongodb.net/test?retryWrites=true&w=majority";
const mongoConnection = new MongoClient(url);
console.log("here");
mongoConnection
    .connect()
    .then(() => {
        console.log("Database Connected");
    })
    .catch((err) => {
        console.log(err);
    });

module.exports = mongoConnection;

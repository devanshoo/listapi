mongoDB = module.exports = require("../index");
Database = module.exports = mongoDB.db("drive8");

const dbCollection = Database.collection("citizen");

const getUserList = async (limit = 5, skip = 0, sortOrder = -1, matchQuery) => {
    limit = parseInt(limit);
    sortOrder = sortOrder === "1" ? 1 : -1;

    const a = await dbCollection
        .aggregate([
            {
                $lookup: {
                    from: "hospital",
                    let: {
                        hospital_Code: "$lastHospitalCode",
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$hospitalCode", "$$hospital_Code"],
                                },
                            },
                        },
                        {
                            $project: {
                                hospitalName: "$name",
                                _id: 0,
                            },
                        },
                    ],
                    as: "hospitalNameArr",
                },
            },
            {
                $unwind: {
                    path: "$vaccinations",
                },
            },
            {
                $lookup: {
                    from: "vaccinationsData",
                    let: {
                        vCode: "$vaccinations.code",
                        date: "$vaccinations.date",
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$code", "$$vCode"],
                                },
                            },
                        },
                        {
                            $project: {
                                _id: 0,
                                vaccineCode: "$code",
                                vaccineName: "$name",
                            },
                        },
                    ],
                    as: "vaccineData",
                },
            },
            {
                $addFields: {
                    hospitalName1: {
                        $arrayElemAt: ["$hospitalNameArr", 0],
                    },
                    gender: {
                        $cond: {
                            if: {
                                $not: {
                                    $in: ["$gender", ["M", "F"]],
                                },
                            },
                            then: "O",
                            else: "$gender",
                        },
                    },
                    ageGroup: {
                        $switch: {
                            branches: [
                                {
                                    case: {
                                        $and: [
                                            {
                                                $gte: ["$age", 18],
                                            },
                                            {
                                                $lte: ["$age", 45],
                                            },
                                        ],
                                    },
                                    then: "adult",
                                },
                                {
                                    case: {
                                        $and: [
                                            {
                                                $gte: ["$age", 46],
                                            },
                                            {
                                                $lte: ["$age", 60],
                                            },
                                        ],
                                    },
                                    then: "middle-aged",
                                },
                                {
                                    case: {
                                        $and: [
                                            {
                                                $gte: ["$age", 61],
                                            },
                                            {
                                                $lte: ["$age", 100],
                                            },
                                        ],
                                    },
                                    then: "senior-citizen",
                                },
                            ],
                            default: "other-age-group.",
                        },
                    },
                    vaccinations: {
                        $arrayElemAt: ["$vaccineData", 0],
                    },
                },
            },
            {
                $sort: {
                    age: sortOrder,
                },
            },
            {
                $group: {
                    _id: "$_id",
                    fullname: {
                        $first: {
                            $concat: ["$firstName", " ", "$lastName"],
                        },
                    },
                    mobileNumber: {
                        $first: "$phoneNo",
                    },
                    hospitalName: {
                        $first: "$hospitalName1.hospitalName",
                    },
                    hospitalCode: {
                        $first: "$lastHospitalCode",
                    },
                    gender: {
                        $first: "$gender",
                    },
                    ageGroup: {
                        $first: "$ageGroup",
                    },
                    vaccinations: {
                        $push: "$vaccinations",
                    },
                },
            },
            {
                $project: {
                    _id: 0,
                },
            },
            {
                $match: matchQuery,
            },
            {
                $limit: limit,
            },
            { $skip: skip },
        ])
        .toArray();

    return a;
};

module.exports = getUserList;

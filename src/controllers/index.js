const getUserList = require("../db/queries/getList");

const getData = async (req, res) => {
    const ageGroup = req.query.ageGroup;
    const hospitalCode = req.query.hospitalCode;
    const vaccineCode = req.query.vaccineCode;
    const gender = req.query.gender;
    let limit = parseInt(req.query.limit) || 5;
    const sortOrder = req.query.sortOrder;
    const keyword = req.query.keyword;
    let page = parseInt(req.query.page) || 0;

    let matchQuery = {};
    if (keyword) {
        matchQuery = {
            $or: [
                {
                    fullname: {
                        $regex: keyword,
                        $options: "i",
                    },
                },
                {
                    hospitalName: {
                        $regex: keyword,
                        $options: "i",
                    },
                },
                {
                    mobileNumber: { $regex: keyword },
                },
            ],
        };
    }
    if (ageGroup) matchQuery.ageGroup = ageGroup;
    if (hospitalCode) matchQuery.hospitalCode = hospitalCode;
    if (vaccineCode) matchQuery["vaccinations.vaccineCode"] = vaccineCode;
    if (gender) matchQuery.gender = gender;

    const skip = limit * page;
    limit = skip + parseInt(limit);

    try {
        const query1 = await getUserList(limit, skip, sortOrder, matchQuery);
        res.send(query1);
    } catch (e) {
        console.log(e);
    }
};

// module.exports = getData;

module.exports.index = getData;

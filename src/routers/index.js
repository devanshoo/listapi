const Router = require("express").Router();
const { index } = require("../controllers");
Router.get("/getUserList", index);
module.exports = Router;

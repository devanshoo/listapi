const express = require("express");
const app = express();
const route = require("./src/routers");
const bodyp = require("body-parser");

mongoDB = module.exports = require("./src/db/index");
Database = module.exports = mongoDB.db("drive8");

app.use(bodyp.urlencoded({ extended: true }));
app.use(express.json());
app.use(route);

app.listen(3000, () => console.log("server connected"));
